public class Tirangle extends Shape {
    private double a;
    private double b;
    private double c;

    public Tirangle(double a, double b, double c) {
        super("Tirangle");
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public double calArea() {
        double s = (a + b + c) / 2;
        return Math.sqrt(s*(s-a)*(s-b)*(s-c));

    }

    @Override
    public double calPerimeter() {
        return a + b + c;

    }

    @Override
    public String toString() {
        return this.getName() + " a:" + this.a + " b:" + this.b + " c:" + this.c;
    }
}
