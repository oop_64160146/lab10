public class App {
    public static void main(String[] args) throws Exception {
        Rectangle rec = new Rectangle(5, 3);
        System.out.println(rec.toString());
        System.out.println(rec.calArea());
        System.out.println(rec.calPerimeter());

        Rectangle rec2 = new Rectangle(2, 2);
        System.out.println(rec2.toString());
        System.out.println(rec2.calArea());
        System.out.println(rec2.calPerimeter());

        Circle circle = new Circle(2);
        System.out.println(circle.toString());
        System.out.printf("%s area: %.3f \n", circle.getName(), circle.calArea());
        System.out.printf("%s perimeter: %.3f \n", circle.getName(), circle.calPerimeter());

        Circle circle2 = new Circle(3);
        System.out.println(circle2.toString());
        System.out.printf("%s area: %.3f \n", circle2.getName(), circle2.calArea());
        System.out.printf("%s perimeter: %.3f \n", circle.getName(), circle2.calPerimeter());

        Tirangle tirangle = new Tirangle(2, 2, 2);
        System.out.println(tirangle.toString());
        System.out.println(tirangle.calArea());
        System.out.println(tirangle.calPerimeter());
    }
}
